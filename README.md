Project 2 by Ryan Moll
Email: rmoll16@gmail.com
This project uses flask to build a simple server. The server should serve hello.html without css, trivia.html with css, and error pages for any other 403 or 404 requests.

Running my flask project
* Go to the web folder in the repository.
* Build the simple flask app image using
  $ docker build -t ryan-final .
* Run the container using
  $ docker run -d -p 5000:5000 ryan-final
* Launch http://127.0.0.1:5000 using web broweser and test the various web pages