from flask import Flask, request, abort, render_template, url_for
import os.path

app = Flask(__name__)

@app.route('/<thing>')
def index(thing):
    if not(thing[0].isalpha()): # Leading character of request was a symbol
        abort(403)
    split_thing = thing.split(".") # Split request on . to seperate filetype
    if (len(split_thing) > 2): # There was more than one . in our request
        #TODO: Make and serve 403 error page
        abort(403)
    if (len(split_thing) == 1): # The request does not have a filetype
        #TODO: Create and serve 404 error page
        abort(404)
    page = split_thing[0]
    for char in page:
        if not char.isalpha(): # If any of the characters in the request are symbols
            abort(403)
    path = './templates/' + thing
    if os.path.isfile(path):
        	return render_template(thing), 200
    abort(404)

@app.errorhandler(404)
def error_404(error):
	return render_template('404.html'), 404
#return a template with error code

@app.errorhandler(403)
def error_403(error):
	return render_template('403.html'), 403
#return a template with error code

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
